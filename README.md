# Ondrej Husar - Kiwi Test Assignment

- [Setup](#setup)
- [Known issues](#known-issues)

## Setup

Clone the repo.

```git
git clone git@gitlab.com:ohusar/kiwi-test-assignment.git
```

Install backend dependencies.

```bash
cd kiwi-test-assignment/be
npm i
npm run start
```

The babel transpilation to dist/ takes few seconds.

```bash
$ npm run start
...

Error: Cannot find module '...kiwi-test-assignment/be/dist'

[nodemon] app crashed - waiting for file changes before starting...
```

But after successfull compilation the server should restart.

```bash

Successfully compiled 10 files with Babel (331ms).
[nodemon] restarting due to changes...

Running at http://localhost:8081/graphql
```

Keep the server running and fire up the frontend server.

```bash
cd kiwi-test-assignment/fe
npm i
npm start
```

Go to [http://localhost:3000](http://localhost:3000) and you should be up and running.

![screen](./screenshot.png)

## Running other things

### FE

#### cypress tests (3)

```
npm run cypress:open
```

#### jest test (1)

```
npm test
```

### BE

#### jest tests (20)

```
npm run test
```

## Known issues

### BE

- The algorithm for word generation is O(3<sup>N</sup>), so after 8+ digit words it gets real slow.

- The word dataset taken from [this GitHub repo](https://github.com/dwyl/english-words) is little bit wierd. For example, it doesn't contain the word 'who' or 'honey', but 'tph' and 'yne' are words apparently. However, contrary to popular autocorrect issues, this one contains word 'fuck' and doesn't contain 'duck'...

- Only works with lowercase words.

- There for sure is a nicer way of doing absolute imports than declaring `alias`-es in `babel.config.js`.

### FE

- `DIGIT_TO_LETTERS` should come from a graphql enum probably (or other BE source)

- For delete, there is only a button, that nukes everything. Partial word / letter deletion is not implemented.

- No real component / unit tests, only cypress tests.

- The call button doesn't actually call anyone, and even if it could it's impossible to write a cell number that contains 0 in it.
