module.exports = (api) => {
  api.cache(false);

  return {
    presets: ["@babel/typescript", ["@babel/env", { targets: { node: "14" } }]],
    plugins: [
      [
        require.resolve("babel-plugin-module-resolver"),
        {
          root: ["./src/"],
          alias: {
            t9: "./src/t9",
            "graphql-schema": "./src/graphql-schema",
            "words-validator": "./src/words-validator",
            "words-prediction": "./src/words-prediction",
            "words-valuator": "./src/words-valuator",
            "db/query": "./src/db/query",
            config: "./src/config",
          },
        },
      ],
      "inline-json-import",
      "@babel/transform-runtime",
    ],
  };
};
