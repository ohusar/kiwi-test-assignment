module.exports = {
  roots: ["./tests"],
  moduleDirectories: ["node_modules", "src"],
  setupFiles: ["./.jest-setup.js"],
};
