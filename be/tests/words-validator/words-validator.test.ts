import { validSubset } from "words-validator";

test("validSubset: empty array", async () => {
  const validWords = await validSubset([]);
  expect(validWords).toEqual([]);
  expect(validWords.length).toEqual(0);
});

test("validSubset: return for invalid word is an empty", async () => {
  const validWords = await validSubset(["xasxaxqxq"]);
  expect(validWords).toEqual([]);
  expect(validWords.length).toEqual(0);
});

test("validSubset: valid words", async () => {
  const existingWords = ["kiwi", "is", "tasty"];
  const validWords = await validSubset(existingWords);

  expect(validWords.length).toEqual(existingWords.length);
  expect(new Set(validWords)).toEqual(new Set(existingWords));
});

test("validSubset: valid & invalid words", async () => {
  const existingWords = ["kiwi", "is", "tasty"];
  const nonExistingWords = ["iwik", "si", "ytsat"];
  const validWords = await validSubset([...existingWords, ...nonExistingWords]);

  expect(validWords.length).toEqual(existingWords.length);
  expect(new Set(validWords)).toEqual(new Set(existingWords));
});

test("validSubset: duplicates are filtered out", async () => {
  const existingWords = ["kiwi", "kiwi", "kiwi"];
  const validWords = await validSubset(existingWords);

  expect(validWords.length).toEqual(1);
});
