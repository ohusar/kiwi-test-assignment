import { sortWordsBasedOnTheirValue } from "words-valuator";

test("sortWordsBasedOnTheirValue: kiwi is first", async () => {
  const sortedWords = sortWordsBasedOnTheirValue(["kiwi", "not", "kiwi"]);
  expect(sortedWords).toEqual(["kiwi", "kiwi", "not"]);
});

test("sortWordsBasedOnTheirValue: kiwi is first", async () => {
  const sortedWords = sortWordsBasedOnTheirValue([
    "asdasd",
    "asd",
    "kiwi",
    "ki",
    "wi",
    "k",
  ]);
  expect(sortedWords[0]).toEqual("kiwi");
});
