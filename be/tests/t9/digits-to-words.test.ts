import { digitsToWords } from "t9";
import { DIGIT_TO_EXPECTED_LENGTH } from "./constants";

test("digitsToWords: return type is Array", () => {
  expect(digitsToWords("")).toBeInstanceOf(Array);
});

test("digitsToWords: empty string returns empty Array", () => {
  expect(digitsToWords("")).toEqual([]);
});

test("digitsToWords: single digits return correct length", () => {
  expect(digitsToWords("1")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["1"]);
  expect(digitsToWords("2")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["2"]);
  expect(digitsToWords("3")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["3"]);
  expect(digitsToWords("4")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["4"]);
  expect(digitsToWords("5")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["5"]);
  expect(digitsToWords("6")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["6"]);
  expect(digitsToWords("7")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["7"]);
  expect(digitsToWords("8")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["8"]);
  expect(digitsToWords("9")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["9"]);
  expect(digitsToWords("0")).toHaveLength(DIGIT_TO_EXPECTED_LENGTH["0"]);
});

test("digitsToWords: 0123456789 combined contain all letters", () => {
  const combined = [
    ...digitsToWords("1"),
    ...digitsToWords("2"),
    ...digitsToWords("3"),
    ...digitsToWords("4"),
    ...digitsToWords("5"),
    ...digitsToWords("6"),
    ...digitsToWords("7"),
    ...digitsToWords("8"),
    ...digitsToWords("9"),
    ...digitsToWords("0"),
  ];

  "abcdefghijklmnopqrstuvwxyz".split("").forEach((letter) => {
    expect(combined).toContain(letter);
  });
});

test("digitsToWords: digit pairs return correct length", () => {
  Object.keys(DIGIT_TO_EXPECTED_LENGTH).forEach((d1) => {
    Object.keys(DIGIT_TO_EXPECTED_LENGTH).forEach((d2) => {
      expect(digitsToWords(`${d1}${d2}`)).toHaveLength(
        DIGIT_TO_EXPECTED_LENGTH[d1] * DIGIT_TO_EXPECTED_LENGTH[d2]
      );
    });
  });
});

test("digitsToWords: wikipedia examples", () => {
  expect(digitsToWords("843")).toContain("the");
  expect(digitsToWords("3673")).toContain("fore");
  expect(digitsToWords("3673")).toContain("ford");
  expect(digitsToWords("3673")).toContain("dose");
});
