export const DIGIT_TO_EXPECTED_LENGTH = {
  "1": 0,
  "2": 3,
  "3": 3,
  "4": 3,
  "5": 3,
  "6": 3,
  "7": 4,
  "8": 3,
  "9": 4,
  "0": 0,
};
