import { addDigitLettersToPrefix } from "t9";

test("addDigitLettersToPrefix: correct instance", () => {
  expect(addDigitLettersToPrefix("0", "prefix")).toBeInstanceOf(Array);
});

test("addDigitLettersToPrefix: throws for 'not digit'", () => {
  expect(() => addDigitLettersToPrefix("X", "prefix")).toThrow();
});

test("addDigitLettersToPrefix: correct length", () => {
  expect(addDigitLettersToPrefix("0", "prefix")).toBeInstanceOf(Array);
});
