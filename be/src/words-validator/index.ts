import { getValidWordsSubset } from "db/query";

export const validSubset = async (words: string[]) => {
  const validWords = await getValidWordsSubset(words);
  return validWords.map((wordRow) => wordRow.word);
};
