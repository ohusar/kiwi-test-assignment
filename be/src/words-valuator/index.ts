export const sortWordsBasedOnTheirValue = (words: string[]) => {
  return words.sort((word1, word2) => {
    const indexOfKiwi1 = word1.indexOf("kiwi");
    const indexOfKiwi2 = word2.indexOf("kiwi");
    return indexOfKiwi2 - indexOfKiwi1;
  });
};
