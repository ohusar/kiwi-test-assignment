import { gql } from "apollo-server-koa";
import { predictWords } from "words-prediction/index";

export const typeDefs = gql`
  type Query {
    predictWords(digits: String): [String]
  }
`;

export const resolvers = {
  Query: {
    predictWords: (ctx, args) => {
      return predictWords(args.digits);
    },
  },
};
