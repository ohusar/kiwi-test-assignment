const DIGIT_TO_LETTERS = {
  "1": [],
  "2": ["a", "b", "c"],
  "3": ["d", "e", "f"],
  "4": ["g", "h", "i"],
  "5": ["j", "k", "l"],
  "6": ["m", "n", "o"],
  "7": ["p", "q", "r", "s"],
  "8": ["t", "u", "v"],
  "9": ["w", "x", "y", "z"],
  "0": [],
};

type Digit = keyof typeof DIGIT_TO_LETTERS;

export const digitsToWords = (digits: string) => {
  if (digits.length === 0) {
    return [];
  }

  let words: string[] = [""];
  for (let i = 0; i < digits.length; i++) {
    const digit = digits[i] as Digit;
    words = addDigitLettersToPrefixes(digit, words);
  }
  return words;
};

export const addDigitLettersToPrefixes = (digit: Digit, prefixes: string[]) => {
  const result: string[] = [];
  prefixes.forEach((prefix) => {
    result.push(...addDigitLettersToPrefix(digit, prefix));
  });
  return result;
};

export const addDigitLettersToPrefix = (digit: string, prefix: string) => {
  const result: string[] = [];
  DIGIT_TO_LETTERS[digit].forEach((letter) => {
    result.push(addLetterToPrefix(letter, prefix));
  });
  return result;
};

export const addLetterToPrefix = (letter: string, prefix: string) => {
  return `${prefix}${letter}`;
};
