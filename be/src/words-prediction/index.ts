import { digitsToWords } from "t9";
import { validSubset } from "words-validator";
import { sortWordsBasedOnTheirValue } from "words-valuator";

export const predictWords = async (digits: string) => {
  const allPossibleWords = digitsToWords(digits);
  const validWords = await validSubset(allPossibleWords);
  const sortedWords = sortWordsBasedOnTheirValue(validWords);
  return sortedWords;
};
