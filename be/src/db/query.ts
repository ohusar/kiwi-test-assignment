import { Knex } from "knex";
import { wordsDb } from "db/types/DbTypes";

import kx from "./db";

const SQLITE_MAX_VARIABLE_NUMBER = 999; // https://www.sqlite.org/limits.html

type PromiseType = Knex.QueryBuilder<wordsDb, any>;

export const getValidWordsSubset = (words: string[]) => {
  const promises: Promise<PromiseType>[] = [];

  for (let i = 0; i < words.length; i += SQLITE_MAX_VARIABLE_NUMBER) {
    const promise = kx<wordsDb>("words")
      .whereIn("word", words.slice(i, i + SQLITE_MAX_VARIABLE_NUMBER))
      .select("word");
    promises.push(promise as any);
  }

  return Promise.all(promises).then((results) => {
    let final = [];
    results.forEach((arr) => {
      final = final.concat(arr);
    });
    return final;
  });
};
