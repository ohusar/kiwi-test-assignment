import { wordsDb } from "db/types/DbTypes";
import kx from "../db";

const words = require("./words.json");

kx<wordsDb>("words")
  .del()
  .then(() => {
    const promises = [];
    for (let i = 0; i <= words.length - 100; i += 100) {
      promises.push(kx<wordsDb>("words").insert(words.slice(i, i + 100)));
    }
    return Promise.all(promises);
  })
  .then(() => {
    console.log("populated db");
  })
  .catch(console.error)
  .finally(() => {
    process.exit();
  });
