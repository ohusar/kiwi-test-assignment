import kx from "../db";

kx.schema
  .hasTable("words")
  .then(function (exists) {
    if (!exists) {
      return kx.schema.createTable("words", function (t) {
        t.increments("id").primary();
        t.string("word", 100);
      });
    }
  })
  .then(() => {
    console.log("schema created");
  })
  .catch(console.error)
  .finally(() => {
    process.exit();
  });
