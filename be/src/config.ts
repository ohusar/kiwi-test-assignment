export const port = Number(process.env.PORT!);
export const nodeEnv = process.env.NODE_ENV || "dev";
export const db = String(process.env.SQLITE_FILENAME!);
