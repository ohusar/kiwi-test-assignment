describe("Keyboard interaction tests", () => {
  it("Basic clicking", () => {
    cy.visit("http://localhost:3000");
    cy.get("[data-cy=2-digit-button]").click();
    cy.get("[data-cy=text]").should("contain", "a");

    cy.get("[data-cy=2-digit-button]").click();
    cy.get("[data-cy=text]").should("contain", "ac");

    cy.get("[data-cy=3-digit-button]").click();
    cy.get("[data-cy=text]").should("contain", "abd");
  });

  it("Only word possibility - 8353746631 (telephone)", () => {
    cy.visit("http://localhost:3000");

    cy.intercept({
      method: "POST",
      url: "http://localhost:8081/graphql",
    }).as("api");

    "835374663".split("").map((d) => {
      cy.get(`[data-cy=${d}-digit-button]`).click();
      cy.wait("@api").then((interception) => {});
    });

    cy.get(`[data-cy=1-digit-button]`).click();
    cy.get("[data-cy=text]").should("contain", "telephone.");
  });

  it("* - word rotation", () => {
    cy.visit("http://localhost:3000");

    "222".split("").forEach((d) => {
      cy.get(`[data-cy=${d}-digit-button]`).click();
    });

    cy.get("[data-cy=text]").should("contain", "abb");
    cy.get(`[data-cy=star-digit-button]`).click();
    cy.get("[data-cy=text]").should("not.contain", "aaa");
  });
});
