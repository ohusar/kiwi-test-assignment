import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};


export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

export type Query = {
  __typename?: 'Query';
  predictWords?: Maybe<Array<Maybe<Scalars['String']>>>;
};


export type QueryPredictWordsArgs = {
  digits?: Maybe<Scalars['String']>;
};

export type PredictWordsQueryVariables = Exact<{
  digits: Scalars['String'];
}>;


export type PredictWordsQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'predictWords'>
);


export const PredictWordsDocument = gql`
    query PredictWords($digits: String!) {
  predictWords(digits: $digits)
}
    `;

/**
 * __usePredictWordsQuery__
 *
 * To run a query within a React component, call `usePredictWordsQuery` and pass it any options that fit your needs.
 * When your component renders, `usePredictWordsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePredictWordsQuery({
 *   variables: {
 *      digits: // value for 'digits'
 *   },
 * });
 */
export function usePredictWordsQuery(baseOptions: Apollo.QueryHookOptions<PredictWordsQuery, PredictWordsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<PredictWordsQuery, PredictWordsQueryVariables>(PredictWordsDocument, options);
      }
export function usePredictWordsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PredictWordsQuery, PredictWordsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<PredictWordsQuery, PredictWordsQueryVariables>(PredictWordsDocument, options);
        }
export type PredictWordsQueryHookResult = ReturnType<typeof usePredictWordsQuery>;
export type PredictWordsLazyQueryHookResult = ReturnType<typeof usePredictWordsLazyQuery>;
export type PredictWordsQueryResult = Apollo.QueryResult<PredictWordsQuery, PredictWordsQueryVariables>;