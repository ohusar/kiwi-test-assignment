// Taken from https://levelup.gitconnected.com/debounce-in-javascript-improve-your-applications-performance-5b01855e086

export const debounce = (func: (...args: any) => any, wait: number) => {
  let timeout: NodeJS.Timeout;

  return function executedFunction(...args: any) {
    const later = () => {
      clearTimeout(timeout);
      console.log("call");
      func(...args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
};
