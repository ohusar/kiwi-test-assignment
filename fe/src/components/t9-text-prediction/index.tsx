import React, { useCallback, useEffect, useState } from "react";

import { usePredictWordsLazyQuery } from "../../generated/graphql";
import { debounce } from "../../utils/debounce";
import BlinkingCursor from "../blinking-cursor";
import Digit from "../digit";
import LoadingCircle from "../loading-circle";
import PhoneUi from "../phone-ui";

import styles from "./t9-text-prediction.module.scss";

const DIGIT_TO_LETTERS = {
  "1": ["."],
  "2": ["a", "b", "c"],
  "3": ["d", "e", "f"],
  "4": ["g", "h", "i"],
  "5": ["j", "k", "l"],
  "6": ["m", "n", "o"],
  "7": ["p", "q", "r", "s"],
  "8": ["t", "u", "v"],
  "9": ["w", "x", "y", "z"],
  "0": [" "],
};

type DigitType = keyof typeof DIGIT_TO_LETTERS;

const T9TextPrediction: React.FC = (props) => {
  const [words, setWords] = useState<string[]>([]);
  const [digits, setDigits] = useState("");
  const [wordIndex, setWordIndex] = useState(0);
  const [wordNotUpToDate, setWordNotUpToDate] = useState(false);

  const [call, { data, loading, called }] = usePredictWordsLazyQuery({
    variables: {
      digits: digits,
    },
    onCompleted: () => {
      setWordNotUpToDate(false);
    },
  });

  const _validWord =
    called &&
    data?.predictWords &&
    data.predictWords.length &&
    data.predictWords[wordIndex];
  const wordIsStale = loading || wordNotUpToDate || !_validWord;

  const onClick = (digit: number) => () => {
    setDigits((prev) => `${prev}${digit}`);
  };

  const onSpace = () => {
    addWord();
    reset();
  };

  const onDot = () => {
    addWordAndDot();
    reset();
  };

  const addWordAndDot = () => {
    addWord(".");
  };

  const addWord = (extra = "") => {
    const nextWords = [...words];
    const toAdd = _validWord ? _validWord : digits;
    nextWords.push(`${toAdd}${extra}`);
    setWords(nextWords);
  };

  const reset = () => {
    call({ variables: { digits: "" } });
    setDigits("");
    setWordIndex(0);
  };

  const del = () => {
    setWords([]);
    reset();
  };

  const changeSelectedWordIndex = () => {
    const length = data?.predictWords?.length;
    if (length) {
      setWordIndex((prev) => (prev + 1) % length);
    }
  };

  const debouncedCall = useCallback(
    debounce((value) => call({ variables: { digits: value } }), 300),
    []
  );

  useEffect(() => {
    if (digits.length) {
      setWordNotUpToDate(true);
      debouncedCall(digits);
    }
  }, [digits, debouncedCall]);

  return (
    <div className={styles.wrapper}>
      <PhoneUi
        text={
          <p className={styles.text} data-cy="text">
            {words.join(" ")} {wordIsStale ? digits : _validWord}
            <BlinkingCursor />
          </p>
        }
        buttons={
          <div className={styles.buttons}>
            <div className={styles.row}>
              <Digit
                onClick={loading ? undefined : onDot}
                label={loading ? <LoadingCircle /> : "1"}
              />
              {[2, 3].map((digit) => (
                <Digit
                  key={digit.toString()}
                  onClick={onClick(digit)}
                  label={digit.toString()}
                  digits={DIGIT_TO_LETTERS[digit.toString() as DigitType]}
                />
              ))}
            </div>
            <div className={styles.row}>
              {[4, 5, 6].map((digit) => (
                <Digit
                  key={digit.toString()}
                  onClick={onClick(digit)}
                  label={digit.toString()}
                  digits={DIGIT_TO_LETTERS[digit.toString() as DigitType]}
                />
              ))}
            </div>
            <div className={styles.row}>
              {[7, 8, 9].map((digit) => (
                <Digit
                  key={digit.toString()}
                  onClick={onClick(digit)}
                  label={digit.toString()}
                  digits={
                    DIGIT_TO_LETTERS[
                      digit.toString() as keyof typeof DIGIT_TO_LETTERS
                    ]
                  }
                />
              ))}
            </div>
            <div className={styles.row}>
              <Digit
                onClick={loading ? undefined : changeSelectedWordIndex}
                label={loading ? <LoadingCircle /> : "*"}
                dataCy="star-digit-button"
                digits={"(rot)".split("")}
              />
              <Digit
                onClick={loading ? undefined : onSpace}
                label={loading ? <LoadingCircle /> : "0"}
                digits={"_".split("")}
              />
              <Digit label="#" onClick={del} digits={"(del)".split("")} />
            </div>
            <div className={`${styles.row} ${styles.centered}`}>
              <Digit label={<span>&#9742;</span>} green={true} />
            </div>
          </div>
        }
      />
    </div>
  );
};

export default T9TextPrediction;
