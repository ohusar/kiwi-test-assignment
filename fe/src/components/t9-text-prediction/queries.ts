import { gql } from "@apollo/client";

export const PREDICT_WORDS = gql`
  query PredictWords($digits: String!) {
    predictWords(digits: $digits)
  }
`;
