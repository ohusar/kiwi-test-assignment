import React from "react";

import styles from "./digit.module.scss";

interface Props {
  label: React.ReactNode;
  digits?: string[];

  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;

  green?: boolean;
  dataCy?: string;
}

const Digit: React.FC<Props> = (props) => {
  const { label, digits, onClick, green, dataCy } = props;

  return (
    <button
      onClick={onClick}
      disabled={!Boolean(onClick)}
      className={`${styles.button} ${green ? styles.green : ""}`}
      data-cy={dataCy ? dataCy : `${label}-digit-button`}
    >
      <div>{label}</div>
      {digits ? <div className={styles.digits}>{digits}</div> : null}
    </button>
  );
};

export default Digit;
