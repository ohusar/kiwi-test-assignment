import React from "react";
import "./blinking-cursor.module.scss";

const BlinkingCursor: React.FC = React.memo(() => {
  return <span className="blinking">|</span>;
});

export default BlinkingCursor;
