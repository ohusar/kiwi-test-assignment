import React from "react";

import styles from "./phone-ui.module.scss";

interface Props {
  buttons: React.ReactNode;
  text: React.ReactNode;
}

const PhoneUi: React.FC<Props> = (props) => (
  <div className={styles.iphone}>
    <div className={styles.text}>{props.text}</div>
    <div className={styles.buttons}>{props.buttons}</div>
  </div>
);

export default PhoneUi;
