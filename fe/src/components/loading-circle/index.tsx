import React from "react";

import "./loading-circle.scss";

const LoadingCircle: React.VFC = () => <div className="loader" />;

export default LoadingCircle;
