import React from "react";
import T9TextPrediction from "./components/t9-text-prediction";

const App = () => {
  return <T9TextPrediction />;
};

export default App;
