import React from "react";
import ReactDOM from "react-dom";
import {
  ApolloProvider,
  HttpLink,
  ApolloClient,
  InMemoryCache,
} from "@apollo/client";

import "./index.css";
import App from "./App";

const link = new HttpLink({
  uri: "http://localhost:8081/graphql",
});
const cache = new InMemoryCache();
const client = new ApolloClient({
  link,
  cache,
  credentials: "include",
  resolvers: {},
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
